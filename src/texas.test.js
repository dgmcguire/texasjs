import Texas from "texasjs"
import {socket} from "../test_helpers/socket_mock"

test('nested attributes update DOM', () => {
  document.body.innerHTML =
    `<ul data-texas="todo_list" class="todo-list" data-static-classes="todo-list"><li class="completed"><div class="view"><input class="toggle" type="checkbox" checked="true"><label>Taste JavaScript</label><button class="destroy"></button></div></li><li class=""><div class="view"><input class="toggle" type="checkbox" checked="false"><label>Buy a unicorn</label><button class="destroy"></button></div></li></ul>`

  let msg = {"todo_list":
    {"children":
      [
        {"eq":2},
        {"add":
          {"data":
            [["li",{"class":""},
              [["div",{"class":"view"},[
                ["input",{"type":"checkbox","class":"toggle","checked":"false"},[]],
                ["label",{},["test"]],
                ["button",{"class":"destroy"},[]]
              ]]]]]}}
      ]
      ,"attrs":[]}}

  let texas = new Texas(socket)
  texas.applyDiff(msg)

  expect(document.body.innerHTML).toEqual(`<ul data-texas="todo_list" class="todo-list" data-static-classes="todo-list"><li class="completed"><div class="view"><input class="toggle" type="checkbox" checked="true"><label>Taste JavaScript</label><button class="destroy"></button></div></li><li class=""><div class="view"><input class="toggle" type="checkbox" checked="false"><label>Buy a unicorn</label><button class="destroy"></button></div></li><li class=""><div class="view"><input type="checkbox" class="toggle" checked="false"><label>test</label><button class="destroy"></button></div></li></ul>`)
});

test('deleting multiple nodes works', () => {
  document.body.innerHTML = `<ul data-texas="test"><li><li></li></li></ul>`
  let msg = {"test":{"children":[{"del":{"count":2}}]}}

  let texas = new Texas(socket)
  texas.applyDiff(msg)

  expect(document.body.innerHTML).toEqual(`<ul data-texas="test"></ul>`)
});
