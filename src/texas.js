import serialize from "./form-serializer"

const TexasDataSelector = "[data-texas]";
const TexasTopic = "texas:main";

export default class Texas {
  constructor(socket){
    this.socketUsable = false
    this.texasMap = this.elemMap()
    this.texasChannel = this.channelInit(socket)
    socket.stateChangeCallbacks.open.push( () => this.socketUsable = true )
    socket.stateChangeCallbacks.open.push( () => {
      this.hijackForms()
    })
    socket.stateChangeCallbacks.close.push( () => this.socketUsable = false )
    socket.stateChangeCallbacks.close.push( () => {
      this.unjackForms()
    })
  }

  hijackForms(){
    for (var form of document.forms) {
      form.addEventListener("submit", (event) => {
        this.formToWebsocketPush(event, this.texasChannel)
      })
    }
  }

  unjackForms(callback){
    for (var form of document.forms) {
      form.removeEventListener("submit", this.callback)
    }
  }

  formToWebsocketPush(event, channel){
    let myFormAttrs = this.getAttrs(event.target)
    let formObj = serialize(event.target, {hash: true})
    let formData = Object.assign(formObj, myFormAttrs)
    channel.push("main", {form_data: formData}, 10000)
      .receive("error", (reasons) => console.log("create failed", reasons) )
      .receive("timeout", () => console.log("Networking issue...") )
    event.target.reset()
    event.preventDefault()
  }

  getAttrs(el) {
    var nodes=[], values=[]
    for (var i = 0; i < el.attributes.length; i++){
      let attrs = el.attributes[i]
      nodes.push(attrs.nodeName)
      values.push(attrs.nodeValue)
    }

    let map = Object.assign({}, ...nodes.map((n, index) => ({[n]: values[index]})))
    return map
  }

  elemMap() {
    let texasElems = document.querySelectorAll(TexasDataSelector)
    let texasMap = new Map()

    texasElems.forEach(
      function(currentElem, currentIndex, listObj){
        let texasKey = currentElem.dataset.texas
        texasMap.set(texasKey, currentElem)
      }
    )
    return texasMap
  };

  channelInit(socket) {
    var channel
    if(socket){
      channel = socket.channel(TexasTopic, {cookies: document.cookie})
      channel.join()
      channel.on("diff", msg => {
        this.applyDiff(msg)
      })
    }
    return channel
  }

  applyDiff(diffObj) {
    let keys = Object.keys(diffObj)
    for(var i in keys){
      let propKey = keys[i]
      let diff = diffObj[propKey]
      if(diff.attrs){ this.updateAttrs(propKey, diff.attrs) }
      if(diff.children){ this.updateChildren(propKey, diff.children) }
    }
  }

  updateAttrs(key, attrsDiff){
    let elem = this.texasMap.get(key)
    let staticClasses = elem.dataset.staticClasses
    for(var i in attrsDiff){
      let script = attrsDiff[i]
      let opType = script[0]
      let op = script[1]
      switch(opType){
        case 'del':
          op = this.cleanStaticOps(op, staticClasses)
          this.removeClass(key, op, staticClasses)
          break;
        case 'add':
          op = this.cleanStaticOps(op, staticClasses)
          this.addClass(elem, op)
          break;
      }
    }
  }

  cleanStaticOps(op, staticClasses){
    let cleanedOps = []
    let classes = op["class"].split(" ")
    for(var i in classes){
      if( !( staticClasses.includes(classes[i]) ) ){
        cleanedOps.push(classes[i])
      }
    }
    return cleanedOps
  }

  updateChildren(key, childrenDiff){
    var elemIndex = 0
    let rootNode = this.texasMap.get(key)

    for(var patchIndex in childrenDiff){
      let patch = childrenDiff[patchIndex]
      let op = Object.keys(patch)[0]

      switch(op){
        case 'eq':
          elemIndex = elemIndex + patch[op];
          break;
        case 'del':
          let del_count = patch[op].count
          this.removeElems(rootNode, elemIndex, del_count)
          break;
        case 'add':
          let count = patch[op].data.length
          this.addElems(elemIndex, rootNode, patch[op].data)
          elemIndex = elemIndex + count
          break;
      }
    }
  }

  addElems(addIndex, rootNode, elemArray){
    let elem
    for( var elemIndex in elemArray ) {
      elem = elemArray[elemIndex]
      if (typeof(elem)==="string") {
        rootNode.insertAdjacentText('beforeend', elem);
      } else {
        this.createTree(addIndex, elem, rootNode)
      }
      addIndex = addIndex+1
    }
  }

  createTree(createIndex, elemObj, parent){
    if (Array.isArray(elemObj) && elemObj.length === 0) { return document.createTextNode("") }
    if (typeof(elemObj)==="string") { return document.createTextNode(elemObj) }

    var elem = document.createElement(elemObj[0])
    elem = this.putAttrs(elem, elemObj[1])
    for(var childIndex in elemObj[2]){
      let childObj = elemObj[2][childIndex]
      let childElem = this.createTree(createIndex, childObj, elem)
      if(childElem.tagName == "FORM"){
        this.myForm = childElem
        this.myFormAttrs = this.getAttrs(childElem)
        childElem.addEventListener("submit", (event) => { this.formToWebsocketPush(event, this.texasChannel) })
      }

      elem.appendChild(childElem)
      parent.insertBefore(elem, parent.children[createIndex])
    }
    return elem
  }

  putAttrs(elem, attrs){
    let keys = Object.keys(attrs)
    for(var keyIndex in keys){
      let key = keys[keyIndex]
      let value = attrs[key]
      elem.setAttribute(key, value)
    }
    return elem
  }

  removeElems(root, index, count){
    for(var i=index;i<(count+index);i++){
      root.childNodes[index].remove()
    }
  }

  addClass(elem, newClasses) {
    let cleanedNew = newClasses.join(' ')
    let classes = elem.className
    let updated = [classes, cleanedNew].join(' ')

    elem.className = updated
  }

  removeClass(key, op, staticClasses) {
    let elem = this.texasMap.get(key)
    let classes = elem.className.split(" ")
    let cleanedClasses = op

    for(var i in op){
      let index = classes.indexOf(op[i]);
      let isNotInStatic = !(staticClasses.includes(op[i]))
      let isInClasses = index > -1
      if ( isInClasses && isNotInStatic )  {
        classes.splice(index, 1);
      }
    }

    elem.className = classes.filter( n => { return n !== "" } ).join(" ")
  }
}
