# texasjs
[![NPM](https://nodei.co/npm/texasjs.png)](https://nodei.co/npm/texasjs/)

Client code for applying patches from [texas](https://gitlab.com/dgmcguire/texas)
