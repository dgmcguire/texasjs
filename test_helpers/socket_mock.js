const channel = {
  join: () => { return null },
  on: (_msg, fn) => { return null }
}

export const socket = {
  stateChangeCallbacks: {
    open: [],
    close: []
  },
  channel: (_topic, _params) => { 
    return channel
  }
}
