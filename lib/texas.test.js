"use strict";

var _texasjs = require("texasjs");

var _texasjs2 = _interopRequireDefault(_texasjs);

var _socket_mock = require("../test_helpers/socket_mock");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

test('nested attributes update DOM', function () {
  document.body.innerHTML = "<ul data-texas=\"todo_list\" class=\"todo-list\" data-static-classes=\"todo-list\"><li class=\"completed\"><div class=\"view\"><input class=\"toggle\" type=\"checkbox\" checked=\"true\"><label>Taste JavaScript</label><button class=\"destroy\"></button></div></li><li class=\"\"><div class=\"view\"><input class=\"toggle\" type=\"checkbox\" checked=\"false\"><label>Buy a unicorn</label><button class=\"destroy\"></button></div></li></ul>";

  var msg = { "todo_list": { "children": [{ "eq": 2 }, { "add": { "data": [["li", { "class": "" }, [["div", { "class": "view" }, [["input", { "type": "checkbox", "class": "toggle", "checked": "false" }, []], ["label", {}, ["test"]], ["button", { "class": "destroy" }, []]]]]]] } }],
      "attrs": [] } };

  var texas = new _texasjs2.default(_socket_mock.socket);
  texas.applyDiff(msg);

  expect(document.body.innerHTML).toEqual("<ul data-texas=\"todo_list\" class=\"todo-list\" data-static-classes=\"todo-list\"><li class=\"completed\"><div class=\"view\"><input class=\"toggle\" type=\"checkbox\" checked=\"true\"><label>Taste JavaScript</label><button class=\"destroy\"></button></div></li><li class=\"\"><div class=\"view\"><input class=\"toggle\" type=\"checkbox\" checked=\"false\"><label>Buy a unicorn</label><button class=\"destroy\"></button></div></li><li class=\"\"><div class=\"view\"><input type=\"checkbox\" class=\"toggle\" checked=\"false\"><label>test</label><button class=\"destroy\"></button></div></li></ul>");
});

test('deleting multiple nodes works', function () {
  document.body.innerHTML = "<ul data-texas=\"test\"><li><li></li></li></ul>";
  var msg = { "test": { "children": [{ "del": { "count": 2 } }] } };

  var texas = new _texasjs2.default(_socket_mock.socket);
  texas.applyDiff(msg);

  expect(document.body.innerHTML).toEqual("<ul data-texas=\"test\"></ul>");
});