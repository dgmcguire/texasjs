"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _formSerializer = require("./form-serializer");

var _formSerializer2 = _interopRequireDefault(_formSerializer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var TexasDataSelector = "[data-texas]";
var TexasTopic = "texas:main";

var Texas = function () {
  function Texas(socket) {
    var _this = this;

    _classCallCheck(this, Texas);

    this.socketUsable = false;
    this.texasMap = this.elemMap();
    this.texasChannel = this.channelInit(socket);
    socket.stateChangeCallbacks.open.push(function () {
      return _this.socketUsable = true;
    });
    socket.stateChangeCallbacks.open.push(function () {
      _this.hijackForms();
    });
    socket.stateChangeCallbacks.close.push(function () {
      return _this.socketUsable = false;
    });
    socket.stateChangeCallbacks.close.push(function () {
      _this.unjackForms();
    });
  }

  _createClass(Texas, [{
    key: "hijackForms",
    value: function hijackForms() {
      var _this2 = this;

      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = document.forms[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var form = _step.value;

          form.addEventListener("submit", function (event) {
            _this2.formToWebsocketPush(event, _this2.texasChannel);
          });
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }
    }
  }, {
    key: "unjackForms",
    value: function unjackForms(callback) {
      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        for (var _iterator2 = document.forms[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          var form = _step2.value;

          form.removeEventListener("submit", this.callback);
        }
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2.return) {
            _iterator2.return();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }
    }
  }, {
    key: "formToWebsocketPush",
    value: function formToWebsocketPush(event, channel) {
      var myFormAttrs = this.getAttrs(event.target);
      var formObj = (0, _formSerializer2.default)(event.target, { hash: true });
      var formData = Object.assign(formObj, myFormAttrs);
      channel.push("main", { form_data: formData }, 10000).receive("error", function (reasons) {
        return console.log("create failed", reasons);
      }).receive("timeout", function () {
        return console.log("Networking issue...");
      });
      event.target.reset();
      event.preventDefault();
    }
  }, {
    key: "getAttrs",
    value: function getAttrs(el) {
      var nodes = [],
          values = [];
      for (var i = 0; i < el.attributes.length; i++) {
        var attrs = el.attributes[i];
        nodes.push(attrs.nodeName);
        values.push(attrs.nodeValue);
      }

      var map = Object.assign.apply(Object, [{}].concat(_toConsumableArray(nodes.map(function (n, index) {
        return _defineProperty({}, n, values[index]);
      }))));
      return map;
    }
  }, {
    key: "elemMap",
    value: function elemMap() {
      var texasElems = document.querySelectorAll(TexasDataSelector);
      var texasMap = new Map();

      texasElems.forEach(function (currentElem, currentIndex, listObj) {
        var texasKey = currentElem.dataset.texas;
        texasMap.set(texasKey, currentElem);
      });
      return texasMap;
    }
  }, {
    key: "channelInit",
    value: function channelInit(socket) {
      var _this3 = this;

      var channel;
      if (socket) {
        channel = socket.channel(TexasTopic, { cookies: document.cookie });
        channel.join();
        channel.on("diff", function (msg) {
          _this3.applyDiff(msg);
        });
      }
      return channel;
    }
  }, {
    key: "applyDiff",
    value: function applyDiff(diffObj) {
      var keys = Object.keys(diffObj);
      for (var i in keys) {
        var propKey = keys[i];
        var diff = diffObj[propKey];
        if (diff.attrs) {
          this.updateAttrs(propKey, diff.attrs);
        }
        if (diff.children) {
          this.updateChildren(propKey, diff.children);
        }
      }
    }
  }, {
    key: "updateAttrs",
    value: function updateAttrs(key, attrsDiff) {
      var elem = this.texasMap.get(key);
      var staticClasses = elem.dataset.staticClasses;
      for (var i in attrsDiff) {
        var script = attrsDiff[i];
        var opType = script[0];
        var op = script[1];
        switch (opType) {
          case 'del':
            op = this.cleanStaticOps(op, staticClasses);
            this.removeClass(key, op, staticClasses);
            break;
          case 'add':
            op = this.cleanStaticOps(op, staticClasses);
            this.addClass(elem, op);
            break;
        }
      }
    }
  }, {
    key: "cleanStaticOps",
    value: function cleanStaticOps(op, staticClasses) {
      var cleanedOps = [];
      var classes = op["class"].split(" ");
      for (var i in classes) {
        if (!staticClasses.includes(classes[i])) {
          cleanedOps.push(classes[i]);
        }
      }
      return cleanedOps;
    }
  }, {
    key: "updateChildren",
    value: function updateChildren(key, childrenDiff) {
      var elemIndex = 0;
      var rootNode = this.texasMap.get(key);

      for (var patchIndex in childrenDiff) {
        var patch = childrenDiff[patchIndex];
        var op = Object.keys(patch)[0];

        switch (op) {
          case 'eq':
            elemIndex = elemIndex + patch[op];
            break;
          case 'del':
            var del_count = patch[op].count;
            this.removeElems(rootNode, elemIndex, del_count);
            break;
          case 'add':
            var count = patch[op].data.length;
            this.addElems(elemIndex, rootNode, patch[op].data);
            elemIndex = elemIndex + count;
            break;
        }
      }
    }
  }, {
    key: "addElems",
    value: function addElems(addIndex, rootNode, elemArray) {
      var elem = void 0;
      for (var elemIndex in elemArray) {
        elem = elemArray[elemIndex];
        if (typeof elem === "string") {
          rootNode.insertAdjacentText('beforeend', elem);
        } else {
          this.createTree(addIndex, elem, rootNode);
        }
        addIndex = addIndex + 1;
      }
    }
  }, {
    key: "createTree",
    value: function createTree(createIndex, elemObj, parent) {
      var _this4 = this;

      if (Array.isArray(elemObj) && elemObj.length === 0) {
        return document.createTextNode("");
      }
      if (typeof elemObj === "string") {
        return document.createTextNode(elemObj);
      }

      var elem = document.createElement(elemObj[0]);
      elem = this.putAttrs(elem, elemObj[1]);
      for (var childIndex in elemObj[2]) {
        var childObj = elemObj[2][childIndex];
        var childElem = this.createTree(createIndex, childObj, elem);
        if (childElem.tagName == "FORM") {
          this.myForm = childElem;
          this.myFormAttrs = this.getAttrs(childElem);
          childElem.addEventListener("submit", function (event) {
            _this4.formToWebsocketPush(event, _this4.texasChannel);
          });
        }

        elem.appendChild(childElem);
        parent.insertBefore(elem, parent.children[createIndex]);
      }
      return elem;
    }
  }, {
    key: "putAttrs",
    value: function putAttrs(elem, attrs) {
      var keys = Object.keys(attrs);
      for (var keyIndex in keys) {
        var key = keys[keyIndex];
        var value = attrs[key];
        elem.setAttribute(key, value);
      }
      return elem;
    }
  }, {
    key: "removeElems",
    value: function removeElems(root, index, count) {
      for (var i = index; i < count + index; i++) {
        root.childNodes[index].remove();
      }
    }
  }, {
    key: "addClass",
    value: function addClass(elem, newClasses) {
      var cleanedNew = newClasses.join(' ');
      var classes = elem.className;
      var updated = [classes, cleanedNew].join(' ');

      elem.className = updated;
    }
  }, {
    key: "removeClass",
    value: function removeClass(key, op, staticClasses) {
      var elem = this.texasMap.get(key);
      var classes = elem.className.split(" ");
      var cleanedClasses = op;

      for (var i in op) {
        var index = classes.indexOf(op[i]);
        var isNotInStatic = !staticClasses.includes(op[i]);
        var isInClasses = index > -1;
        if (isInClasses && isNotInStatic) {
          classes.splice(index, 1);
        }
      }

      elem.className = classes.filter(function (n) {
        return n !== "";
      }).join(" ");
    }
  }]);

  return Texas;
}();

exports.default = Texas;